package endpoint;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.TransactionReceipt;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ListenerServletFrom extends HttpServlet {
    public volatile static ArrayList<String> txFromBlackList = new ArrayList<>();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            EthBlock.Block block = ListenerServlet.web3j.ethGetBlockByNumber(DefaultBlockParameterName.LATEST, true).sendAsync().get().getBlock();
            System.out.println("BLOCK HASH: " + block.getHash() + " BLOCK NUMBER: " + block.getNumber());
            List<EthBlock.TransactionResult> list = block.getTransactions();
            for (EthBlock.TransactionResult result : list) {
                JSONObject jsonObject = new JSONObject(result);
                String hash = jsonObject.getString("hash");
                String to = null;
                String from = null;
                BigInteger value = jsonObject.getBigInteger("value");
                try {
                    from = jsonObject.getString("from");
                } catch (JSONException e) {
                    from = null;
                }
                try {
                    to = jsonObject.getString("to");
                } catch (JSONException e) {
                    to = null;
                }

                TransactionReceipt receipt = null;
                if (ListenerServlet.addressList.contains(from)) {
                    System.out.println("Catched-FROM Complete--------------1----------------");
//                    System.out.println("Catched Complete!!");
                    receipt = ListenerServlet.web3j.ethGetTransactionReceipt(hash).sendAsync().get().getResult();
                    if (receipt == null) System.out.println("receipt-FROM null");
                    int status = 0;
                    System.out.println("Catched-FROM Complete--------------2----------------");

                    if (!txFromBlackList.contains(hash)) {
                        txFromBlackList.add(hash);
                        if (receipt!=null){
                            if ("0x1".equalsIgnoreCase(receipt.getStatus())) status = 1;
                            if ("0x0".equalsIgnoreCase(receipt.getStatus())) status = 2;
                        }

                        System.out.println("tx hash for sender: " + hash + " Size of blacklist: " + txFromBlackList.size());
                        HashMap<String, Object> data = new HashMap<>();
                        data.put("from", from);
                        data.put("to", to);
                        data.put("status", status);
                        data.put("hash", hash);
                        data.put("amount", value);
                        System.out.println("Catched-FROM Complete--------------3----------------");

                        try {
                            String rsp = Jsoup.connect(ListenerServlet.URL_NOTI_SENDER)
                                    .ignoreContentType(true)
                                    .method(Connection.Method.POST)
                                    .requestBody(ListenerServlet.util.gson.toJson(data)).ignoreHttpErrors(true)
                                    .execute()
                                    .body();
                            System.out.println("RSP-FROM: " + rsp);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Catched-FROM Complete--------------4----------------");

                    }
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();

        }
    }
}
