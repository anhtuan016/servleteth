package endpoint;

import entity.BalanceChange;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import util.RESTUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class PendingListenerServlet extends HttpServlet {
    private static Logger LOGGER = Logger.getLogger("PENDING-ListenerServlet: ---");
    public static ArrayList<String> txPendingBlackList = new ArrayList<>();
    public static ArrayList<String> txFromPendingBlackList = new ArrayList<>();
    public final String URL_NOTI_RECEIVER = "https://1-dot-digitalwalletservice.appspot.com/v2/notifications/eth?status=pending";
    public final String URL_NOTI_SENDER = "https://1-dot-digitalwalletservice.appspot.com/v2/notisender/eth?status=pending";
    public final static String MAINNET_URL = "https://mainnet.infura.io/P24zANq8jUry0WgHRcXd";
    public final static String TESTNET_URL = "https://rinkeby.infura.io/P24zANq8jUry0WgHRcXd";
    public static Web3j web3j = Web3j.build(new HttpService(MAINNET_URL));

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        try {
            EthBlock.Block block = web3j.ethGetBlockByNumber(DefaultBlockParameterName.PENDING, true).sendAsync().get().getBlock();
            System.out.println("BLOCK NUMBER: " + block.getNumber());
            List<EthBlock.TransactionResult> list = block.getTransactions();
            for (EthBlock.TransactionResult result : list) {
                JSONObject jsonObject = new JSONObject(result);
                String hash = jsonObject.getString("hash");
                String to = null;
                String from = null;
                BigInteger value = jsonObject.getBigInteger("value");
                try {
                    from = jsonObject.getString("from");
                } catch (JSONException e) {
                    from = null;
                }
                try {
                    to = jsonObject.getString("to");
                } catch (JSONException e) {
                    to = null;
                }
                if (ListenerServlet.addressList.contains(to)) {
                    System.out.println("Catched Pending!!");
                    int status = 0;
                    if (!txPendingBlackList.contains(hash)) {
                        txPendingBlackList.add(hash);
                        System.out.println("Tx-PENDING for receiver valid: " + hash);
                        final String to1 = to;
                        final String from1 = from;
                        final int status1 = status;
                        executorService.submit(new Runnable() {
                            @Override
                            public void run() {
                                BalanceChange balanceChange = new BalanceChange(to1, value, status1);
                                balanceChange.setHash(hash);
                                balanceChange.setSender_Address(from1);
                                try {
                                    String rsp = Jsoup.connect(URL_NOTI_RECEIVER)
                                            .ignoreContentType(true)
                                            .method(Connection.Method.POST)
                                            .requestBody(RESTUtil.gson.toJson(balanceChange))
                                            .execute()
                                            .body();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
                if (ListenerServlet.addressList.contains(from)) {
                    System.out.println("Catched Pending!!");
                    int status = 0;
                    if (!txFromPendingBlackList.contains(hash)) {
                        txFromPendingBlackList.add(hash);
                        LOGGER.info("pending-tx hash for sender: " + hash + " Size of blacklist: " + txFromPendingBlackList.size());
                        HashMap<String, Object> data = new HashMap<>();
                        data.put("from", from);
                        data.put("to", to);
                        data.put("status", status);
                        data.put("hash", hash);
                        data.put("amount", value);
                        try {
                            String rsp = Jsoup.connect(URL_NOTI_SENDER)
                                    .ignoreContentType(true)
                                    .method(Connection.Method.POST)
                                    .requestBody(RESTUtil.gson.toJson(data))
                                    .execute()
                                    .body();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
