package endpoint;

import entity.CronEntity;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CronServlet extends HttpServlet {

    static {
        try {
            SchedulerFactory schedulerFactory = new StdSchedulerFactory();
            Scheduler sched = schedulerFactory.getScheduler();
            sched.start();
            JobDetail job = JobBuilder.newJob(CronEntity.class).withIdentity("myJob", "group1").build();
            Trigger trigger = TriggerBuilder.newTrigger().withIdentity("myTrigger", "group1")
                    .startNow()
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(3).repeatForever())
                    .build();
            sched.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Cron Started!");

    }
}
