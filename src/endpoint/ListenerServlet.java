package endpoint;

import com.fasterxml.jackson.databind.util.JSONPObject;
import entity.BalanceChange;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthBlockNumber;
import org.web3j.protocol.core.methods.response.Transaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import rx.Subscription;
import util.RESTUtil;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class ListenerServlet extends HttpServlet {
    private static Logger LOGGER = Logger.getLogger("ListenerServlet: ---");
    public static ArrayList<String> addressList = new ArrayList<>();
    static RESTUtil util = new RESTUtil();
    public static Web3j web3j;
    public volatile static ArrayList<String> txBlackList = new ArrayList<>();
    public final static String MAINNET_URL = "https://mainnet.infura.io/P24zANq8jUry0WgHRcXd";
//    public final static String TESTNET_URL = "https://rinkeby.infura.io/P24zANq8jUry0WgHRcXd";
//    public static String url = "http://54.39.22.4:8545";
//    public static String url = "https://mew.giveth.io";

    public final String URL_NOTI_RECEIVER = "https://1-dot-digitalwalletservice.appspot.com/v2/notifications/eth?status=complete";
    public static final String URL_NOTI_SENDER = "https://1-dot-digitalwalletservice.appspot.com/v2/notisender/eth?status=complete";

    static {
        int i = 1;
        web3j = Web3j.build(new HttpService(MAINNET_URL));
//      Load address from GAE
        boolean flag = true;
        while (flag) {
            LOGGER.info("Time: " + i);
            if (addressList.size() != 0) {
                LOGGER.info("Break LOOP");
                flag = false;
                break;
            }
            try {
                if (addressList.size() == 0) {
                    String content = Jsoup.connect("https://1-dot-digitalwalletservice.appspot.com/allethaddress")
                            .ignoreContentType(true).timeout(60 * 1000)
                            .method(Connection.Method.GET)
                            .execute()
                            .body();
                    addressList = util.gson.fromJson(content, ArrayList.class);
                    System.out.println("ADDRESS LIST SIZE: " + addressList.size());
                    LOGGER.info("Break LOOP");
                    flag = false;
                }
            } catch (IOException e) {
                System.out.println("Times " + i + " Exception at stage: - Get All ETH Address in static blocks: " + e.getMessage());
                i++;
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String newAddress = req.getParameter("address");
        if (newAddress != null && newAddress.length() > 0) {
            LOGGER.info("New Address: " + newAddress);
            LOGGER.info("Address List Size: " + addressList.size());
            addressList.add(newAddress);
            return;
        }

        String remove = req.getParameter("remove");
        if ("true".equals(remove)) {
            addressList = new ArrayList<>();
            LOGGER.info("Reinitialize List Address, new Size: " + addressList.size());
            resp.getWriter().print("Reinitialize List Address, new Size: " + addressList.size());
            return;
        }
        String loadList = req.getParameter("reload");
        if ("true".equals(loadList)) {
            String content = Jsoup.connect("https://1-dot-digitalwalletservice.appspot.com/v2/allethaddress")
                    .ignoreContentType(true)
                    .method(Connection.Method.GET)
                    .execute()
                    .body();
            addressList = util.gson.fromJson(content, ArrayList.class);
            LOGGER.info("List is get successfully!");
            resp.getWriter().print("List is get successfully!");
            return;
        }

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().print("Servlet Initialized successfully");
        LOGGER.info("DONE");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        try {
            EthBlock.Block block = web3j.ethGetBlockByNumber(DefaultBlockParameterName.LATEST, true).sendAsync().get().getBlock();
            System.out.println("BLOCK HASH: " + block.getHash() + " BLOCK NUMBER: " + block.getNumber());
            List<EthBlock.TransactionResult> list = block.getTransactions();
            for (EthBlock.TransactionResult result : list) {
                JSONObject jsonObject = new JSONObject(result);
                String hash = jsonObject.getString("hash");
                String to = null;
                String from = null;
                BigInteger value = jsonObject.getBigInteger("value");
                try {
                    from = jsonObject.getString("from");
                } catch (JSONException e) {
                    from = null;
                }
                try {
                    to = jsonObject.getString("to");
                } catch (JSONException e) {
                    to = null;
                }
                TransactionReceipt receipt = null;
                if (addressList.contains(to)) {
                    System.out.println("Catched Complete!!");
                    receipt = web3j.ethGetTransactionReceipt(hash).sendAsync().get().getResult();
                    if (receipt == null) System.out.println("receipt-TO null");
                    System.out.println("Catched Complete--------------1----------------");

                    int status = 1;
                    if (!txBlackList.contains(hash)) {
                        txBlackList.add(hash);
                        LOGGER.info("tx hash for receiver: " + hash + " Size of blacklist: " + txBlackList.size());

                        System.out.println("Catched Complete--------------2----------------");
                        if (receipt != null) {
                            if ("0x1".equalsIgnoreCase(receipt.getStatus())) status = 1;
                            if ("0x0".equalsIgnoreCase(receipt.getStatus())) status = 2;
                        }

                        System.out.println("COMPLETE-Tx hash receiver: " + hash);
//                        final String to1 = to;
//                        final int status1 = status;
                        System.out.println("Catched Complete--------------3----------------");

//                        executorService.submit(new Runnable() {
//                            @Override
//                            public void run() {
                        BalanceChange balanceChange = new BalanceChange(to, value, status);
                        balanceChange.setHash(hash);
                        balanceChange.setSender_Address(from);
                        try {
                            String rsp = Jsoup.connect(URL_NOTI_RECEIVER)
                                    .ignoreContentType(true)
                                    .method(Connection.Method.POST)
                                    .requestBody(util.gson.toJson(balanceChange)).ignoreHttpErrors(true)
                                    .execute()
                                    .body();
                            System.out.println("RSP-TO: " + rsp);
                            System.out.println("Catched Complete--------------4----------------");
                            System.out.println("DATA SENT: "+RESTUtil.gson.toJson(balanceChange));

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//                            }
//                        });
                    }
                }

            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }


    }
}
