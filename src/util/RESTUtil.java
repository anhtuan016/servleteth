package util;

import com.google.gson.Gson;

import java.io.*;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class RESTUtil {
    private Logger logger = Logger.getLogger(RESTUtil.class.getSimpleName());
    public String blacklistTxDir = "/geth/blacklistTx.txt";
    public String blacklistTxPending = "/geth/blacklistTxPending.txt";
    private FileReader fileReader;
    private BufferedReader bufferedReader;
    private FileWriter fileWriter;
    private BufferedWriter bufferedWriter;

    private FileReader fileReaderPending;
    private BufferedReader bufferedReaderPending;
    private FileWriter fileWriterPending;
    private BufferedWriter bufferedWriterPending;
    public static Gson gson = new Gson();

    public boolean isDupplicateTx(String tx) {
        try {
            File file = new File(blacklistTxDir);
            if (!file.exists()) file.createNewFile();
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String line;
            StringBuffer stringBuffer = new StringBuffer();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }
            String txArray[] = stringBuffer.toString().split("/");
            List<String> txList = Arrays.asList(txArray);
            return txList.contains(tx);


        } catch (IOException e) {
            e.printStackTrace();
            return true;
        } finally {
            try {
                if (bufferedReader != null) bufferedReader.close();

            } catch (IOException e) {
                System.out.println("Exception at finally block");
                e.printStackTrace();
            }
        }
    }

    public void addToBlackListTx(String tx) {
        try {
            File file = new File(blacklistTxDir);
            if (!file.exists()) file.createNewFile();
            fileWriter = new FileWriter(file, true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("/");
            bufferedWriter.write(tx);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedWriter != null) bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Exception at finally");
            }
        }
    }

    public boolean isDupplicateTxPending(String tx) {
        try {
            File file = new File(blacklistTxPending);
            if (!file.exists()) file.createNewFile();
            fileReaderPending = new FileReader(file);
            bufferedReaderPending = new BufferedReader(fileReaderPending);
            String line;
            StringBuffer stringBuffer = new StringBuffer();
            while ((line = bufferedReaderPending.readLine()) != null) {
                stringBuffer.append(line);
            }
            String txArray[] = stringBuffer.toString().split("/");
            List<String> txList = Arrays.asList(txArray);
            return txList.contains(tx);


        } catch (IOException e) {
            e.printStackTrace();
            return true;
        } finally {
            try {
                if (bufferedReaderPending != null) bufferedReaderPending.close();
                if (fileReaderPending != null) fileReaderPending.close();
            } catch (IOException e) {
                System.out.println("Exception at finally block");
                e.printStackTrace();
            }
        }
    }

    public void addToBlackListTxPending(String tx) {
        try {
            File file = new File(blacklistTxPending);
            if (!file.exists()) file.createNewFile();
            fileWriterPending = new FileWriter(file, true);
            bufferedWriterPending = new BufferedWriter(fileWriterPending);
            bufferedWriterPending.write("/");
            bufferedWriterPending.write(tx);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedWriterPending != null) bufferedWriterPending.close();
                if (fileWriterPending != null) fileWriterPending.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Exception at finally");
            }
        }
    }

    public static BigInteger hexaToBigInteger(String hexa) {
        BigInteger bi = new BigInteger(hexa.substring(2), 16);
        return bi;
    }

}
