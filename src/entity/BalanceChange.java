package entity;

import java.math.BigInteger;


public class BalanceChange {
    private String address;
    private BigInteger amount;
    private int status; //0-pending ,1-success,2-failed.
    private String hash;
    private String sender_Address;

    public BalanceChange(String address, BigInteger amount, int status) {
        this.address = address;
        this.amount = amount;
        this.status = status;
    }

    public BalanceChange() {

    }

    public BalanceChange(String address, BigInteger amount) {

        this.address = address;
        this.amount = amount;
    }

    public String getSender_Address() {
        return sender_Address;
    }

    public void setSender_Address(String sender_Address) {
        this.sender_Address = sender_Address;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}


