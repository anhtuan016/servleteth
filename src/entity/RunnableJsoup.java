package entity;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;

public class RunnableJsoup implements Runnable {
    String url;

    public RunnableJsoup() {
    }

    public RunnableJsoup(String url) {
        this.url = url;
    }

    @Override
    public void run() {
        try {
            String rsp = Jsoup.connect(url).method(Connection.Method.POST).ignoreHttpErrors(true).ignoreContentType(true).execute().body();
            System.out.println("RSP RUNNALBE: " + rsp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
