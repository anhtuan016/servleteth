package entity;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.IOException;
import java.util.concurrent.*;

public class CronEntity implements Job {
    private static String URL = "http://localhost:8080/servleteth/add";
    private static String LISTEN_FROM = "http://localhost:8080/servleteth/listenfrom/";
    private static String PENDING_URL = "http://localhost:8080/servleteth/pending";
    static ExecutorService executorService = new ThreadPoolExecutor(8,16,3,TimeUnit.SECONDS,new LinkedBlockingQueue<Runnable>());

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        RunnableJsoup jsoup_pending = new RunnableJsoup(PENDING_URL);
        RunnableJsoup jsoup_completeTo = new RunnableJsoup(URL);
        RunnableJsoup jsoup_completeFrom = new RunnableJsoup(LISTEN_FROM);
        System.out.println("Cron Run...");
        executorService.execute(jsoup_pending);
        executorService.execute(jsoup_completeTo);
        executorService.execute(jsoup_completeFrom);


    }
}
